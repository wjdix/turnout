defmodule Turnout.DecayingSwitchTest do
  use ExUnit.Case, async: true
  alias Turnout.DecayingSwitch
  alias Turnout.Support.GenEventSpy

  setup do
    {:ok, manager} = GenEvent.start_link

    {:ok, manager: manager}
  end

  test "enters the received error state after receiving an error", %{manager: manager} do
    received_failure = fn(event) ->
      GenEvent.sync_notify(manager, {:receive_error, event})
    end

    over_threshold = fn(event) ->
      GenEvent.sync_notify(manager, {:over_threshold, event})
    end

    task = GenEventSpy.spy(manager, fn messages ->
      Enum.take(messages, 1)
    end)

    {:ok, switch} = DecayingSwitch.start_link(%{n: 1,
                                        received_failure: received_failure,
                                        over_threshold: over_threshold})

    DecayingSwitch.send_event(switch, {:failure, %{}})

    assert [{:receive_error, _}] = Task.await(task)
  end

  test "enters the over_threshold state after receiving N errors", %{manager: manager} do
    task = GenEventSpy.spy(manager, fn messages ->
      Enum.take(messages, 1)
    end)

    over_threshold = fn(event) ->
      GenEvent.sync_notify(manager, {:over_threshold, event})
    end

    {:ok, switch} = DecayingSwitch.start_link(%{n: 9,
                                        duration: 60000,
                                        over_threshold: over_threshold,
                                       })

    Enum.map(1..11, fn(_) ->
      DecayingSwitch.send_event(switch, {:failure, %{}})
    end)

    assert [{:over_threshold, _}] = Task.await(task)
  end

  test "events decay after X time", %{manager: manager} do
    over_threshold = fn(x) ->
      GenEvent.sync_notify(manager, {:over_threshold, x})
    end

    received_failure = fn(x) ->
      GenEvent.sync_notify(manager, {:received_failure, x})
    end

    {:ok, switch} = DecayingSwitch.start_link(%{
          duration: 600,
          received_failure: received_failure,
          over_threshold: over_threshold})

    now = Timex.Time.epoch(:secs)

    task = GenEventSpy.spy(manager, fn messages ->
      messages
      |> Enum.take(11)
      |> Enum.map fn x -> elem(x, 0) end
    end)

    Enum.map(1..9, fn(_) ->
      DecayingSwitch.send_event(switch, {:failure, %{}}, now)
    end)

    new_now = now + (60 * 11)

    DecayingSwitch.send_event(switch, {:failure, %{}}, new_now)
    DecayingSwitch.send_event(switch, {:failure, %{}}, new_now)

    events = Enum.map(1..11, fn _ -> :received_failure end)

    assert events == Task.await(task)
  end
end
