defmodule Turnout.Support.GenEventSpy do
  def spy(pid, func) do
    starting_count = handler_count(pid)
    stream = GenEvent.stream(pid)

    task = Task.async(fn ->
      func.(stream)
    end)

    wait_for_handlers(pid, starting_count + 1)

    task
  end

  defp handler_count(pid) do
    pid |> GenEvent.which_handlers |> length
  end

  defp wait_for_handlers(pid, count) do
    unless handler_count(pid) == count do
      wait_for_handlers(pid, count)
    end
  end
end
