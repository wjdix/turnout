defmodule Turnout.GraveStoneCounter do
  def init() do
    %{counter: 0, gravestones: []}
  end

  def increment(counter, now, duration) do
    %{counter: counter, gravestones: stones} = reap(counter, now)
    %{counter: counter + 1, gravestones: [now + duration | stones]}
  end

  def count(counter, now) do
    reap(counter, now).counter
  end

  defp reap(%{counter: counter, gravestones: gravestones}, now) do
    {_, alive} = Enum.partition gravestones, fn x -> x < now end
    %{ counter: length(alive), gravestones: alive}
  end
end
