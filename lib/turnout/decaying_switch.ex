defmodule Turnout.DecayingSwitch do
  @moduledoc """
  Provides a switch that will trip after a configurable number of events.
  Events will decay over time and allow the switch to go back to the untripped state.
  Functions can be provided to act on events in each of the states.
  """
  alias Turnout.GraveStoneCounter

  def start_link(config \\ %{}, opts \\ []) do
    :gen_fsm.start_link(__MODULE__, [config], opts)
  end

  def send_event(switch, {message, dimensions}, time \\ Timex.Time.epoch(:secs)) do
    :gen_fsm.send_event(switch, {message, dimensions, time})
  end

  def get_state(switch) do
    {state, _} = :sys.get_state(switch)
    {:ok , state}
  end

  def defaults do
    %{n: 10,
      counter: GraveStoneCounter.init(),
      received_failure: fn(_) -> end,
      over_threshold: fn(_) -> end,
      duration: 600}
  end

  def init([config]) do
    {:ok, :idle, Dict.merge(defaults, config)}
  end

  def idle({:failure, event, time}, state) do
    state.received_failure.(event)
    new_counter = GraveStoneCounter.increment(state.counter, time, state.duration)

    {:next_state, :received_failure, %{state | counter: new_counter}}
  end

  def received_failure({:failure, event, time}, state) do
    state.received_failure.(event)
    new_counter = GraveStoneCounter.increment(state.counter, time, state.duration)
    next_state = if GraveStoneCounter.count(new_counter, time) >= state.n do
      :over_threshold
    else
      :received_failure
    end

    {:next_state, next_state, %{state | counter: new_counter}}
  end

  def over_threshold({:failure, event, time}, state) do
    state.over_threshold.(event)
    new_counter = GraveStoneCounter.increment(state.counter, time)

    {:next_state, :over_threshold, %{state | counter: new_counter}}
  end
end
